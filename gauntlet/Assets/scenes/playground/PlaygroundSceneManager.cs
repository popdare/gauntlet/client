//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace blacktriangles
{
    public class PlaygroundSceneManager
        : SceneManager
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum GlobalAction
        {
            Inventory,
            CloseWindow,
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public static new PlaygroundSceneManager instance       { get { return SceneManager.instance as PlaygroundSceneManager; } }

        [Header("Player Settings")]
        public Character characterPrefab                        = null;
        public CharacterLoadout loadout                         = null;

        [Header("Enemy Settings")]
        public Character enemyPrefab                            = null;
        public int enemyCount                                   = 10;

        [Header("Environment Settings")]
        public GameObject environment;
        [ReadOnly] public Bounds bounds;

        [Header("Interface")]
        public ArmoryPanel armoryPanel;

        [Header("State")]
        [ReadOnly] public Character localCharacter              = null;
        [ReadOnly] public List<Character> enemies               = new List<Character>();

        private InputRouter<GlobalAction> input;

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void SpawnPlayer()
        {
            if(localCharacter == null)
            {
                localCharacter = Character.Spawn(characterPrefab, Vector3.up, Quaternion.identity, Character.Type.Local, 0);
            }

            localCharacter.Initialize();
            localCharacter.datamodel.armory.Equip(loadout);

            RequestCamera(localCharacter);
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator SpawnEnemies()
        {
            float area = bounds.extents.magnitude;
            var wait = new WaitForSeconds(0.1f);

            while(true)
            {

                enemies.RemoveAll((character)=>{
                    return !character.datamodel.state.isAlive;
                });

                if(enemies.Count < enemyCount)
                {
                    Vector3 randomPoint = btRandom.RandomPointInCircle(bounds.center, 100f);
                    NavMeshHit hit;
                    if(NavMesh.SamplePosition(randomPoint, out hit, 100f, NavMesh.AllAreas))
                    {
                        Character enemy = Character.Spawn(enemyPrefab, hit.position, Quaternion.identity);
                        enemy.Initialize();
                        enemies.Add(enemy);
                    }   
                }

                yield return wait;
            }
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            GameManager.instance.CaptureCursor();
            input = new InputRouter<GlobalAction>();
            input.Bind(GlobalAction.Inventory, InputAction.FromKey(KeyCode.Tab));
            input.Bind(GlobalAction.CloseWindow, InputAction.FromKey(KeyCode.Escape));

            uiActive = false;
            
            bounds = environment.CalculateBounds();
            SpawnPlayer();
            StartCoroutine(SpawnEnemies());
            NotifySceneReady();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if(localCharacter == null) return;
            if(input.GetKeyPressed(GlobalAction.Inventory))
            {
                if(armoryPanel.state.visible.active)
                {
                    HideArmory();
                }
                else
                {
                    ShowArmory();
                }
            }

            if(input.GetKeyPressed(GlobalAction.CloseWindow))
            {
                if(armoryPanel.state.visible.active)
                {
                    HideArmory();
                }
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void ShowArmory()
        {
            armoryPanel.Show(localCharacter.datamodel.armory, 1f);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void HideArmory()
        {
            armoryPanel.Hide(1f, null);
        }
    }
}
