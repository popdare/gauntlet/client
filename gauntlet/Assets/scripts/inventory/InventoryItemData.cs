//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public enum InventoryItemType
    {
        Equipment,
        Weapon,
        Ammo,
        Magazine,
        Usable,
        Craft,
        Treasure
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public abstract class InventoryItemData
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public abstract InventoryItemType type                  { get; }

        public string id                                        { get { return name; } }
        public Sprite icon                                      { get { return _icon; } }
        public string displayName                               { get { return _displayName; } }
        public string description                               { get { return _description; } }
        public bool hasInventory                                { get { return inventorySettings.capacity > 0; } }
        public int stackSize                                    { get { return _stackSize; } }
        public int volume                                       { get { return _volume; } }
        public Inventory.Settings inventorySettings             { get { return _inventorySettings; } }
        
        [Header("Display")]
        [SerializeField] Sprite _icon                           = null;
        [SerializeField] string _displayName                    = System.String.Empty;
        [SerializeField, TextArea(2,8)] string _description     = System.String.Empty;

        [Header("Inventory")]
        [SerializeField, Min(1)] int _stackSize                 = 1;
        [SerializeField, Min(0)] int _volume                    = 0;
        [SerializeField] Inventory.Settings _inventorySettings  = default(Inventory.Settings);
        
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public virtual bool IsRestricted(InventoryItem item)
        {
            return ((int)item.data.type & inventorySettings.typeRestrictions) > 0;
        }
    }
}
