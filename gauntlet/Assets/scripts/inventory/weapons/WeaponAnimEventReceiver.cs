//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public enum WeaponAnimEventType
    {
        DrawFinished,
        HolsterFinished,
        ReloadFinished,
    }

    //
    // ########################################################################
    //
    

    public class WeaponAnimEventReceiver
        : MonoBehaviour
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void WeaponAnimEventCallback(Animator animator, WeaponAnimEventType type);
        public event WeaponAnimEventCallback OnWeaponAnimEvent;
        
        //
        // callbacks //////////////////////////////////////////////////////////
        //

        public void HandleAnimEvent(Animator animator, WeaponAnimEventType ev)
        {
            WeaponAnimEventCallback cb = OnWeaponAnimEvent;
            if(cb != null)
            {
                cb(animator, ev);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Spawn(GameObject obj)
        {
            GlobalGameObjectPool.Take(obj, transform.position, transform.rotation, null);
        }
        
    }
}
