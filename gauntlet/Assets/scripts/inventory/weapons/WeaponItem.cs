//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class WeaponItem
        : InventoryItem
        , IStatsModifier
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public new WeaponItemData data                          { get { return base.data as WeaponItemData; } }

        public InventoryItem magazine                           { get; private set; }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public WeaponItem(Character character, WeaponItemData data, int count)
            : base(character, data, count)
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public WeaponController CreateController(Character character)
        {
            WeaponController result = GameObject.Instantiate(data.prefab) as WeaponController;
            result.Initialize(character, this);
            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool CanLoadMagazine(InventoryItem mag)
        {
            if(mag == null) return false;
            if(mag.data.type != InventoryItemType.Magazine) return false;

            MagazineItemData magdata = mag.data as MagazineItemData;
            if(magdata.ammoType != data.ammoType) return false;

            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public InventoryItem LoadMagazine(InventoryItem mag)
        {
            InventoryItem result = null;
            if(CanLoadMagazine(mag))
            {
                result = magazine;
                magazine = mag;
            }

            return result;
        }

        //
        // IStatsModifier /////////////////////////////////////////////////////
        //
        
        public CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr)
        {
            return data.CalculateStats(orig, curr);
        }
    }
}
