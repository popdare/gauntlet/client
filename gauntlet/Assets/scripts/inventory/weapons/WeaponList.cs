//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;

namespace blacktriangles
{
    [System.Serializable]
    public class WeaponDataList
        : EnumList<WeaponSlot, WeaponItemData>
    {
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    [System.Serializable]
    public class WeaponList
        : EnumListNoSerde<WeaponSlot, WeaponItem>
    {
    }
}

