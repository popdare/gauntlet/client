//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public enum AmmoType
    {
        Ammo22,
        Ammo380acp,
        Ammo9mm,
        Ammo45acp,
        Ammo556x45mm,
        Ammo762x51,
        Ammo50cal,
        Ammo12gague,
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public static class AmmoTypeUtil
    {
        public static string GetDisplayName(AmmoType type)
        {
            switch(type)
            {
                case AmmoType.Ammo22:                           return ".22";
                case AmmoType.Ammo380acp:                       return ".380 ACP";
                case AmmoType.Ammo9mm:                          return "9mm";
                case AmmoType.Ammo45acp:                        return ".45 ACP";
                case AmmoType.Ammo762x51:                       return "762x51";
                case AmmoType.Ammo556x45mm:                     return "556x45";
                case AmmoType.Ammo50cal:                        return ".50";
                case AmmoType.Ammo12gague:                      return "12 Gague";
            }

            return null;
        }
    }
}
