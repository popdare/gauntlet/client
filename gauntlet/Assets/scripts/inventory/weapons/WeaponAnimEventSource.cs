//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class WeaponAnimEventSource
        : StateMachineBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public bool fireOnEnter;
        [ShowIf("fireOnEnter")] public WeaponAnimEventType enter;
        public bool fireOnExit;
        [ShowIf("fireOnExit")] public WeaponAnimEventType exit;
        public bool fireOnFinished;
        [ShowIf("fireOnFinished")] public WeaponAnimEventType finished;

        private WeaponAnimEventReceiver _receiver                = null;
        
        //
        // private methods ////////////////////////////////////////////////////
        //

        private WeaponAnimEventReceiver GetReceiver(Animator animator)
        {
            if(_receiver == null)
            {
                _receiver = animator.GetComponent<WeaponAnimEventReceiver>();
                Dbg.Assert(_receiver != null, $"No WeaponAnimEventReceiver found on {animator.name}");
            }

            return _receiver;
        }

        //
        // --------------------------------------------------------------------
        //

        private void FireEvent(Animator animator, WeaponAnimEventType type)
        {
            WeaponAnimEventReceiver receiver = GetReceiver(animator);
            receiver.HandleAnimEvent(animator, type);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator WaitForFinished(Animator animator, AnimatorStateInfo info, WeaponAnimEventType type)
        {
            if(info.speedMultiplier != 0f)
            {
                yield return new WaitForSeconds(info.length * (1f / info.speedMultiplier) );
            }

            FireEvent(animator, type);
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layerIdx)
        {
            if(fireOnEnter)
            {
                FireEvent(animator, enter);
            }

            if(fireOnFinished)
            {
                WeaponAnimEventReceiver receiver = GetReceiver(animator);
                receiver.StartCoroutine(WaitForFinished(animator, info, finished));
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public override void OnStateExit(Animator animator, AnimatorStateInfo info, int layerIdx)
        {
            if(fireOnExit)
            {
                FireEvent(animator, exit);
            }
        }
    }
}
