//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public enum WeaponItemType
    {
        Primary,
        Sidearm,
        Utility
    };

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    [CreateAssetMenu(fileName="WeaponItem", menuName="blacktriangles/items/Weapon")]
    public class WeaponItemData
        : InventoryItemData
        , IStatsModifier
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override InventoryItemType type                  { get { return InventoryItemType.Weapon; } }
        public WeaponItemType weaponType                        { get { return _weaponType; } }
        public WeaponController prefab                          { get { return _prefab; } }

        [Header("CONTROLLER")]
        [SerializeField] WeaponItemType _weaponType             = WeaponItemType.Primary;
        [SerializeField] WeaponController _prefab               = null;

        [Header("WEAPON STATS")]
        public AmmoType ammoType                                = AmmoType.Ammo9mm;
        public bool usesMagazine                                = true;
        public GameObject primaryFireFx                         = null;
        public SprayPattern sprayPattern                        = null;

        [ShowIf("IsHitScanWeapon")] public HitScanWeaponController.Settings hitScanSettings = HitScanWeaponController.Settings.Default;
        public StatsModifierList modifiers                      = new StatsModifierList();

        //
        // editor only methods ////////////////////////////////////////////////
        //
        #if UNITY_EDITOR
        
        private bool IsHitScanWeapon()
        {
            if(_prefab != null)
            {
                HitScanWeaponController cont = _prefab.GetComponent<HitScanWeaponController>();
                return cont != null;
            }

            return false;
        }

        #endif

        //
        // IStatsCalculator ///////////////////////////////////////////////////
        //

        public CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr)
        {
            return modifiers.Apply(curr);
        }
    }
}
