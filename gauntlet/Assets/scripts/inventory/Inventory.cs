//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class Inventory
        : IEnumerable<InventoryItem>
    {
        //
        // struct /////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Settings
        {
            [Min(0)] public int capacity;
            [EnumFlag(typeof(InventoryItemType))] public int typeRestrictions;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Settings settings                                { get; private set; }
        public int capacity                                     { get { return settings.capacity; } }
        public int used                                         { get; private set; }
        public int remaining                                    { get { return capacity - used; } }
        public int itemCount                                    { get { return GetItemCount(); } } 
        public float percRemaining                              { get { return capacity == 0 ? 0 : remaining / (float)capacity; } }

        [SerializeField] private List<InventoryItem> items      = new List<InventoryItem>();

        private System.Func<InventoryItem, bool> isRestricted;

        //
        // constructor / initializer //////////////////////////////////////////
        //
        
        public Inventory(Settings settings, System.Func<InventoryItem, bool> isRestricted)
        {
            Dbg.Assert(isRestricted != null, "Inventory REQUIRES a function to determine whether or not an item is restricted");
            this.settings = settings;
            this.isRestricted = isRestricted;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool Contains(InventoryItem item)
        {
            return items.Contains(item);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool Add(InventoryItem item)
        {
            Dbg.Assert(item != null, "Add called with null item");
            
            bool result = false;
            if(!isRestricted(item) && item.totalVolume <= remaining)
            {
                used += item.totalVolume;
                items.Add(item);
                result = true;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public InventoryItem Pop()
        {
            if(items.Count > 0)
            {
                InventoryItem item = items[0];
                if(item.count == 0)
                {
                    items.Pop();
                }

                if(item.count > 1)
                {
                    used -= item.data.volume;
                    --item.count;
                    return InventoryItem.Create(item.character, item.data, 1);
                }
                else
                {
                    used -= item.data.volume;
                    return items.Pop();
                }
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Remove(InventoryItem item)
        {
            Dbg.Assert(item != null, "Remove called with null item");
            if(items.Contains(item))
            {
                items.Remove(item);
                used -= item.totalVolume;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override string ToString()
        {
            return $"Capacity[{capacity}] Used[{used}] itemCount[{itemCount}]";
        }

        //
        // IEnumerable ////////////////////////////////////////////////////////
        //

        public IEnumerator<InventoryItem> GetEnumerator()
        {
            items.RemoveAll((t)=>t.count == 0);
            foreach(InventoryItem t in items)
            {
                yield return t;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        public int GetItemCount()
        {
            int result = 0;
            foreach(InventoryItem item in items)
            {
                result += item.count;
            }
            return result;
        }
    }
}
