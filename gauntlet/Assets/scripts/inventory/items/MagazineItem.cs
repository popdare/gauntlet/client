//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public class MagazineItem
        : InventoryItem
    {
        public new MagazineItemData data                        { get { return base.data as MagazineItemData; } }

        //
        // constructor / initializer ///////////////////////////////////////////
        //

        public MagazineItem(Character character, MagazineItemData data, int count)
            : base(character, data, count)
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Combine(InventoryItem other)
        {
            bool result = false;
            if(!data.IsRestricted(other))
            {
                result = true;
                if(character != null)
                {
                    character.controller.PackMagazine(this, other);
                }
                else
                {
                    inventory.Add(other);
                }
            }

            return result;
        }
    }
}
