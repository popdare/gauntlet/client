//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="Magazine", menuName="blacktriangles/items/Magazine")]
    public class MagazineItemData
        : InventoryItemData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public override InventoryItemType type                  { get { return InventoryItemType.Magazine; } }

        [Header("Magazine")]
        public AmmoType ammoType                                = AmmoType.Ammo22;
        public float ammoPackingSpeed                           = 1f;

        [Header("Visuals")]
        public GameObject reloadObj                             = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool IsRestricted(InventoryItem item)
        {
            InventoryItemData data = item.data;
            if(data.type != InventoryItemType.Ammo)
                return true;

            AmmoItemData ammoData = data as AmmoItemData;
            if(ammoData == false)
                return true;

            return ammoData.ammoType != ammoType;
        }
    }
}
