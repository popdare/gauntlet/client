//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="Ammo", menuName="blacktriangles/items/Ammo")]
    public class AmmoItemData
        : InventoryItemData
    {
        public override InventoryItemType type                  { get { return InventoryItemType.Ammo; } }

        [Header("Ammo")]
        public AmmoType ammoType                                = AmmoType.Ammo22;
    }
}
