//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public class InventoryItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Character character                              { get; private set; }
        public InventoryItemData data                           { get; private set; }
        public Inventory inventory                              { get; private set; }
        public int count                                        = 0;
        public int totalVolume                                  { get { return count * data.volume; } }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public InventoryItem(Character character, InventoryItemData data, int count)
        {
            this.character = character;
            this.data = data;
            inventory = new Inventory(data.inventorySettings, data.IsRestricted);
            this.count = btMath.Clamp(count, 0, data.stackSize);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static InventoryItem Create(Character character, InventoryItemData data, int count)
        {
            if(data == null) return null;

            switch(data.type)
            {
                case InventoryItemType.Equipment: return new EquipmentItem(character, data as EquipmentItemData, count);
                case InventoryItemType.Weapon: return new WeaponItem(character, data as WeaponItemData, count);
                case InventoryItemType.Magazine: return new MagazineItem(character, data as MagazineItemData, count);
                default:
                    return new InventoryItem(character, data, count);
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual bool Combine(InventoryItem other)
        {
            return false;
        }
    }
}
