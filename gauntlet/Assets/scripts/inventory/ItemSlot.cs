//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public enum EquipSlot
    {
        Head,
        Torso,
        Back,
        Waist,
        Feet,
        Hands
    }

    //
    // ------------------------------------------------------------------------
    //
    
    public enum WeaponSlot
    {
        Primary,
        Secondary,
        Sidearm,
        Utility1,
        Utility2,
        Utility3
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public static class ItemSlotUtils
    {
        public static string DisplayName(EquipSlot slot)
        {
            switch(slot)
            {
                case EquipSlot.Head:    return "Head";
                case EquipSlot.Torso:   return "Torso";
                case EquipSlot.Back:    return "Back";
                case EquipSlot.Waist:   return "Waist";
                case EquipSlot.Feet:    return "Feet";
                case EquipSlot.Hands:   return "Hands";
            }

            return slot.ToString();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static string DisplayName(WeaponSlot slot)
        {
            switch(slot)
            {
                case WeaponSlot.Primary:    return "Primary";
                case WeaponSlot.Secondary:  return "Secondary";
                case WeaponSlot.Sidearm:    return "Sidearm";
                case WeaponSlot.Utility1:   return "Utility #1";
                case WeaponSlot.Utility2:   return "Utility #2";
                case WeaponSlot.Utility3:   return "Utility #3";

            }
            return slot.ToString();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static bool IsCompatible(WeaponItemType type, WeaponSlot slot)
        {
            switch(slot)
            {
                case WeaponSlot.Primary:        return type == WeaponItemType.Primary;
                case WeaponSlot.Secondary:      return type == WeaponItemType.Primary;
                case WeaponSlot.Sidearm:        return type == WeaponItemType.Sidearm;
                case WeaponSlot.Utility1:       return type == WeaponItemType.Utility;
                case WeaponSlot.Utility2:       return type == WeaponItemType.Utility;
                case WeaponSlot.Utility3:       return type == WeaponItemType.Utility;
            }

            return false;
        }
    }
}
