//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class Armory
        : IStatsModifier
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Character character                              { get; private set; }
        public EquipmentList equipment                          = new EquipmentList();
        public WeaponList weapons                               = new WeaponList();

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public Armory(Character character)
        {
            this.character = character;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Equip(CharacterLoadout loadout)
        {
            //
            // equip items
            //

            EquipmentItem lastEquip = null;
            loadout.equipment.ForEach((EquipSlot slot, EquipmentItemData data)=>{
                if(data != null)
                {
                    lastEquip = new EquipmentItem(character, data, 1);
                    InternalEquip(lastEquip);
                }
            });

            //
            // equip weapons
            //
            
            loadout.weapons.ForEach((WeaponSlot slot, WeaponItemData data)=>{
                if(data != null)
                {
                    var lastWeapon = new WeaponItem(character, data, 1);
                    InternalEquip(lastWeapon, slot);
                }
            });

            //
            // add items to inventory (held items)
            //

            List<InventoryItem> addedItems = new List<InventoryItem>();
            foreach(CharacterLoadout.HeldItem held in loadout.heldItems)
            {
                InventoryItem newItem = InventoryItem.Create(character, held.data, held.count);
                if(newItem == null) 
                    continue;

                EquipmentItem item = equipment[held.slot];
                if(addedItems.IsValidIndex(held.inventoryIdx))
                {
                    InventoryItem container = addedItems[held.inventoryIdx];
                    container.inventory.Add(newItem);
                }
                else
                {
                    item.inventory.Add(newItem);
                    addedItems.Add(newItem);
                }
            }

            //
            // load weapons
            //

            foreach(CharacterLoadout.WeaponLoad load in loadout.loads)
            {
                WeaponItem weap = weapons[load.slot];
                if(weap != null)
                {
                    InventoryItem mag = InventoryItem.Create(character, load.magazine, 1);
                    if(mag != null)
                    {
                        InventoryItem ammo = InventoryItem.Create(character, load.ammo, load.ammoCount);
                        if(ammo != null)
                        {
                            mag.inventory.Add(ammo);
                            weap.LoadMagazine(mag);
                        }
                    }
                }
            }

            //
            // finalize
            // 

            character.datamodel.events.NotifyEquipmentChanged(character, null, lastEquip);
            character.controller.DrawAnyWeapon(true);
        }

        //
        // --------------------------------------------------------------------
        //

        public EquipmentList Equip(EquipmentList newEquipment)
        {
            EquipmentList old = new EquipmentList();
            newEquipment.ForEach((e,item)=>{
                old[e] = InternalEquip(newEquipment[e]);
            });

            return old;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public WeaponList Equip(WeaponList newWeapons)
        {
            WeaponList old = new WeaponList();
            newWeapons.ForEach((e,item)=>{
                old[e] = InternalEquip(newWeapons[e], e);
            });

            return old;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public EquipmentItem Equip(EquipmentItem item)
        {
            EquipmentItem old = InternalEquip(item);
            character.datamodel.events.NotifyEquipmentChanged(character, old, item);
            return old;
        }

        //
        // --------------------------------------------------------------------
        //

        public WeaponItem Equip(WeaponItem item, WeaponSlot slot)
        {
            WeaponItem old = InternalEquip(item, slot);
            character.datamodel.events.NotifyEquipmentChanged(character, old, item);
            return old;
        }
        

        //
        // --------------------------------------------------------------------
        //

        public EquipmentItem Unequip(EquipSlot slot)
        {
            EquipmentItem old = InternalUnequip(slot);
            character.datamodel.events.NotifyEquipmentChanged(character, old, null);
            return old;
        }

        //
        // --------------------------------------------------------------------
        //

        public WeaponItem Unequip(WeaponSlot slot)
        {
            WeaponItem old = InternalUnequip(slot);
            character.datamodel.events.NotifyEquipmentChanged(character, old, null);
            return old;
        }

        //
        // IStatsCalculator ///////////////////////////////////////////////////
        //

        public CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr)
        {
            if(equipment != null)
            {
                foreach(EquipmentItem item in equipment)
                {
                    if(item != null)
                    {
                        curr = item.CalculateStats(orig, curr);
                    }
                }
            }

            if(weapons != null)
            {
                foreach(WeaponItem item in weapons)
                {
                    if(item != null)
                    {
                        curr = item.CalculateStats(orig, curr);
                    }
                }
            }

            return curr;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private EquipmentItem InternalEquip(EquipmentItem item)
        {
            if(item == null) return null;

            EquipmentItem old = equipment[item.data.slot];
            equipment[item.data.slot] = item;
            return old;
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        private EquipmentItem InternalUnequip(EquipSlot slot)
        {
            EquipmentItem old = equipment[slot];
            equipment[slot] = null;
            return old;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private WeaponItem InternalEquip(WeaponItem item, WeaponSlot slot)
        {
            if(item == null) return null;
            if(ItemSlotUtils.IsCompatible(item.data.weaponType, slot) == false) return null;

            WeaponItem old = weapons[slot];
            weapons[slot] = item;
            return old;
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        private WeaponItem InternalUnequip(WeaponSlot slot)
        {
            WeaponItem old = weapons[slot];
            weapons[slot] = null;
            return old;
        }
    }
}
