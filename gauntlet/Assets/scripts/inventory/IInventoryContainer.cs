#if FALSE
//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public interface IInventoryContainer
    {
        Inventory GetInventory();
    }
}
#endif
