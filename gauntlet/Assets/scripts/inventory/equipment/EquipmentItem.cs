//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public class EquipmentItem
        : InventoryItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public new EquipmentItemData data                       { get { return base.data as EquipmentItemData; } }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public EquipmentItem(Character character, EquipmentItemData data, int count)
            : base(character, data, count)
        {
        }

        //
        // IStatsModifier /////////////////////////////////////////////////////
        //
        
        public CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr)
        {
            return data.CalculateStats(orig, curr);
        }
    }
}
