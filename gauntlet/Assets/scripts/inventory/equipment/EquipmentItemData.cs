//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="EquipmentItem", menuName="blacktriangles/items/Equipment")]
    public class EquipmentItemData
        : InventoryItemData
        , IStatsModifier
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public override InventoryItemType type                  { get { return InventoryItemType.Equipment; } }
        public EquipSlot slot                                   { get { return _slot; } }

        [SerializeField] EquipSlot _slot                        = EquipSlot.Torso;
        public StatsModifierList modifiers;

        //
        // public methos //////////////////////////////////////////////////////
        //

        public virtual CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr)
        {
            return modifiers.Apply(curr);
        }
    }
}
