//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;

namespace blacktriangles
{
    [System.Serializable]
    public class EquipmentDataList
        : EnumList<EquipSlot, EquipmentItemData>
    {
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    [System.Serializable]
    public class EquipmentList
        : EnumListNoSerde<EquipSlot, EquipmentItem>
    {
    }
}
