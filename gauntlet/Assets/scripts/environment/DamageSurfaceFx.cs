//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public enum DamageSurfaceType
    {
        Default,
        Flesh,
        Metal,
        Wood,
        Dirt,
        Rock
    }


    [System.Serializable]
    public class DamageSurfaceFx
        : EnumList<DamageSurfaceType, GameObject>
    {
    }
}
