//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

#if AGRANBURG_ASTAR

using blacktriangles;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    [System.Serializable]
    public struct GridGraphSettings
    {
        public Bounds bounds;
    }

    //
    // ------------------------------------------------------------------------
    //
    
 
    public static class AStarDataExtension
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static T AddGraph<T>(this AstarData self)
            where T: NavGraph
        {
            return self.AddGraph(typeof(T)) as T;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static T FindGraph<T>(this AstarData self)
            where T: NavGraph 
        {
            return self.FindGraphOfType(typeof(T)) as T;
        }
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public static class AStarPathExtension
    {
        public static IEnumerator BuildRecast( 
            this AstarPath navi, 
            System.Action<RecastGraph> cb = null,
            RecastGraph graph = null)
        {
            AstarData data =navi.data;
            if(graph == null)
            {
                graph = data.FindGraph<RecastGraph>();
            }

            graph.SnapForceBoundsToScene();
            graph.forcedBoundsSize += Vector3.one;

            AstarPath.active.Scan();

            yield return navi.WaitForAstarScan();

            if(cb != null)
            {
                cb(graph);
            }
        }

        //
        // ------------------------------------------------------------------------
        //
        
        public static IEnumerator BuildGridGraph( 
            this AstarPath navi, 
            GridGraphSettings settings, 
            System.Action<GridGraph> cb = null,
            GridGraph graph = null)
        {
            AstarData data = navi.data;
            if(graph == null)
            {
                graph = data.FindGraph<GridGraph>();
            }

            graph.width = (int)Mathf.Ceil( settings.bounds.size.x / graph.nodeSize );
            graph.depth = (int)Mathf.Ceil( settings.bounds.size.z / graph.nodeSize );
            graph.SetDimensions(graph.width, graph.depth, graph.nodeSize);

            AstarPath.active.Scan();
            yield return navi.WaitForAstarScan();

            if(cb != null)
            {
                cb(graph);
            }
        }

        //
        // ------------------------------------------------------------------------
        //
        
        private static IEnumerator WaitForAstarScan( this AstarPath navi )
        {
            yield return new WaitForSeconds(0.1f);
            while(AstarPath.active.isScanning)
                yield return new WaitForSeconds(0.1f);
        }
    }
}

#endif
