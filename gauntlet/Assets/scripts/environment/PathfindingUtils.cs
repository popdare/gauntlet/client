//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

#if AGRANBURG_ASTAR

using blacktriangles;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public static class Utils
    {
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static List<Mesh> CollectGroundMeshes()
        {
            Mesh[] allGos = GameObject.FindObjectsOfType<Mesh>();
            List<Mesh> result = allGos.Filter((Mesh m)=>{
                return false;
            });

            Dbg.Log(result.Count.ToString());
            return result;
        }
    }
}

#endif
