//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace blacktriangles
{
    public class InventorySlotListItem
        : UIListItem
        , IDropHandler
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public InventoryItem item                               { get { return data as InventoryItem; } }
        public bool isValid                                     { get { return item != null || item.data != null; } }

        public Image icon;
        public TextMeshProUGUI count;
        public TextMeshProUGUI inventoryCount;
        public UISimpleMeter inventoryMeter;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            count.gameObject.SetActive(false);
            inventoryMeter.gameObject.SetActive(false);
            inventoryCount.gameObject.SetActive(false);

            if(isValid)
            {
                icon.sprite = item.data.icon;
                if(item.count > 1)
                {
                    count.gameObject.SetActive(true);
                    count.text = $"{item.count}";
                }

                if(item.inventory != null && item.inventory.capacity > 0)
                {
                    inventoryCount.gameObject.SetActive(true);
                    inventoryCount.text = $"{item.inventory.itemCount}";
                    inventoryMeter.gameObject.SetActive(true);
                    inventoryMeter.SetValue(1f-item.inventory.percRemaining);
                }
            }
            else
            {
                icon.sprite = null;
            }

            return true;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override UIElement MakeDragDisplay()
        {
            if(!isValid) return null;

            UIElement ele = base.MakeDragDisplay();
            Image img = ele.GetComponent<Image>();
            img.sprite = item.data.icon;
            return ele;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnDrop(PointerEventData ev)
        {
            GameObject go = ev.pointerDrag;
            InventorySlotListItem dropped = go.GetComponent<InventorySlotListItem>();
            if(dropped != null)
            {
                item.Combine(dropped.item);
            }
        }
    }
}
