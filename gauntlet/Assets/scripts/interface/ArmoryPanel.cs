//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace blacktriangles
{
    public class ArmoryPanel
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Armory armory                                    { get; private set; }
        public UIList weaponList;
        public UIList equipmentList;

        public static ArmoryPanel activePanel                   { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Show(Armory armory, float time)
        {
            this.armory = armory;
            Show(time, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            base.Refresh();
            if(armory == null) return false;

            //
            // set weapon data
            //
            var wdata = new List<WeaponItemListItem.Data>();
            armory.weapons.ForEach((slot, item)=>{
                wdata.Add(new WeaponItemListItem.Data() {
                    item = item,
                    slot = slot,
                });
            });
            weaponList.SetData(wdata);
            
            //
            // set equipment data
            //
            var edata = new List<EquipmentItemListItem.Data>();
            armory.equipment.ForEach((slot, item)=> {
                edata.Add(new EquipmentItemListItem.Data() {
                    item = item,
                    slot = slot,
                });
            });
            equipmentList.SetData(edata);

            return true;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void StartShow()
        {
            armory.character.audio.Play(AudioTrigger.OpenInventory);
            GameManager.instance.ReleaseCursor();
            SceneManager.instance.uiActive = true;
            activePanel = this;
            base.StartShow();
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void StartHide()
        {
            armory.character.audio.Play(AudioTrigger.CloseInventory);
            GameManager.instance.CaptureCursor();
            SceneManager.instance.uiActive = false;
            activePanel = null;
            base.StartHide();
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void NotifyOnShow()
        {
            base.NotifyOnShow();
            StartCoroutine(RefreshUpdate());
        }
        

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RefreshUpdate()
        {
            var wait = new WaitForSeconds(0.1f);
            while(state.visible.active)
            {
                yield return wait;
                Refresh();
            }
        }
        
    }
}
