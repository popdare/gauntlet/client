//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class EquipmentItemListItem
        : UIListItem
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Data
        {
            public EquipmentItem item                           = null;
            public EquipSlot slot                               = EquipSlot.Head;
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Data datum                                       { get { return data as Data; } }
        public bool isValid                                     { get { return datum.item != null && datum.item.data != null; } }

        [Header("Settings")]
        public Sprite emptySprite;

        [Header("Elements")]
        public UIList slotList;
        public Image icon;
        public TextMeshProUGUI slotName;
        public TextMeshProUGUI itemName;
        public UISimpleMeter containerFillMeter;
        public TextMeshProUGUI containerFillText;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            slotName.text = ItemSlotUtils.DisplayName(datum.slot);
            if(isValid)
            {
                EquipmentItem item = datum.item;
                icon.sprite = item.data.icon;
                itemName.text = item.data.displayName;
                containerFillMeter.SetValue(1f-item.inventory.percRemaining);
                containerFillText.text = $"{item.inventory.used}/{item.inventory.capacity}";

                slotList.SetData(item.inventory);
            }
            else
            {
                icon.sprite = emptySprite;
                itemName.text = "[None]";
                containerFillMeter.SetValue(0f);
                containerFillText.text = $"0/0";
            }

            return true;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override UIElement MakeDragDisplay()
        {
            if(!isValid) return null;

            UIElement ele = base.MakeDragDisplay();
            Image img = ele.GetComponent<Image>();
            img.sprite = datum.item.data.icon;
            return ele;
        }
    }
}
