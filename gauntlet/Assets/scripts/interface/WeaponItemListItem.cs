//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class WeaponItemListItem
        : UIListItem
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Data
        {
            public WeaponItem item                              = null;
            public WeaponSlot slot                              = WeaponSlot.Primary;
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Data datum                                       { get { return data as Data; } }

        [Header("Settings")]
        public Sprite emptySprite;

        [Header("Elements")]
        public Image icon;
        public TextMeshProUGUI slotName;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            slotName.text = ItemSlotUtils.DisplayName(datum.slot);

            if(datum.item == null || datum.item.data == null)
            {
                icon.sprite = emptySprite;
            }
            else
            {
                WeaponItemData item = datum.item.data;
                icon.sprite = item.icon;
            }

            return true;
        }
    }
}
