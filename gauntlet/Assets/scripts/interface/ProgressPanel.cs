//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public class ProgressPanel
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UISimpleMeter outerMeter;
        public UISimpleMeter innerMeter;

        public static ProgressPanel activePanel                 { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void SetProgress(float progress)
        {
            if(progress > 0f)
            {
                Show();
            }
            else
            {
                Hide();
            }

            outerMeter.gameObject.SetActive(false);
            innerMeter.SetValue(progress);
        }

        //
        // --------------------------------------------------------------------
        //

        public void SetProgress(float innerProg, float outerProg)
        {
            if(innerProg > 0f || outerProg > 0f)
            {
                Show();
            }
            else
            {
                Hide();
            }

            innerMeter.SetValue(innerProg);
            outerMeter.gameObject.SetActive(true);
            outerMeter.SetValue(outerProg);
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            activePanel = this;
        }
    }
}
