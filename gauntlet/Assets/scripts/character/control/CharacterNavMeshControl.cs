//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.AI;

namespace blacktriangles 
{
    public class CharacterNavMeshControl
        : CharacterControl
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public NavMeshAgent agent                               { get; private set; }

        //
        // initialize /////////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);
            agent = GetComponent<NavMeshAgent>();
            agent.enabled = true;

            CharacterStats stats = datamodel.stats;
            agent.speed = stats.moveSpeed;
            agent.height = stats.height;
        }


        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void SetDestination(Vector3 target)
        {
            agent.SetDestination(target);
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Update()
        {
            CharacterState state = datamodel.state;
            Vector3 velocity = agent.velocity;
            state.movedir.Set(velocity);
            state.isMoving.Set(velocity.sqrMagnitude > 0f);

            Vector3 diff = agent.steeringTarget - character.transform.position;
            state.lookdir = diff.normalized;
            state.isGrounded.Set(true);

            UpdateState();
            agent.isStopped = !state.isAlive;
        }
        
    }
}
