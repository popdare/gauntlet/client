//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class CharacterControl
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public CharacterController unityController              { get; private set; }

        public DamageArea[] damageAreas                         = null;

        //
        // initialize /////////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);
            unityController = GetComponent<CharacterController>();
            foreach(DamageArea area in damageAreas)
            {
                area.OnTakeDamage += OnTakeDamage;
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        //
        // locomotion =========================================================
        //

        public void Teleport(Vector3 pos)
        {
            unityController.enabled = false;
            transform.position = pos;
            unityController.enabled = true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Move(Vector2 dir)
        {
            Vector3 move = character.transform.TransformDirection(dir.ToVector3XZ());
            MoveAbs(move);
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public void MoveAbs(Vector3 dir)
        {
            datamodel.state.movedir.Add(dir);
            bool moving = datamodel.state.movedir.smoothed.sqrMagnitude > 0f;
            if(moving != datamodel.state.isMoving)
            {
                datamodel.state.isMoving.Set(moving);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Look(Vector2 delta)
        {
            CharacterStats stats = datamodel.stats;
            CharacterState state = datamodel.state;

            delta = new Vector2(delta.x*stats.lookSpeed.x, delta.y*stats.lookSpeed.y);
            state.lookdir += delta;
            state.lookdir.x = state.lookdir.x % 360f;
            state.lookdir.y = Mathf.Clamp(state.lookdir.y, -90f, 90f);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void LookTowards(Vector3 dir)
        {
            Quaternion desiredRot = Quaternion.LookRotation(dir, Vector3.up);
            datamodel.state.lookdir = new Vector2(desiredRot.eulerAngles.y, desiredRot.eulerAngles.x);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Jump()
        {
            if(datamodel.state.isGrounded && !datamodel.state.isJumping)
            {
                character.audio.Play(AudioTrigger.Jump);
                datamodel.state.isJumping.Set(true);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Walk(bool walk)
        {
            if(datamodel.state.isWalking != walk)
            {
                datamodel.state.isWalking.Set(walk);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ToggleCrouch()
        {
            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            state.isCrouching.Set(!(bool)datamodel.state.isCrouching);
            if(state.isCrouching)
            {
                unityController.height = stats.crouchHeight;
            }
            else
            {
                unityController.height = stats.height;
            }

            unityController.center = new Vector3(0f, unityController.height / 2f, 0f);
        }

        //
        // --------------------------------------------------------------------
        //

        public void HardStop()
        {
            datamodel.state.movedir.Set(Vector2.zero);
        }

        //
        // weapons ============================================================
        //

        public void SetAimDownSights(bool aim)
        {
            if(datamodel.state.isAiming != aim)
            {
                Abort();
                datamodel.state.isAiming.Set(aim);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void DrawAnyWeapon(bool force = false)
        {
            WeaponList.Entry entry = datamodel.armory.weapons.First((e, weapon)=>{
                return weapon != null;
            });

			if(entry != null && entry.item != null)
			{
                DrawWeapon(entry.slot, force);
			}
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void DrawWeapon(WeaponSlot slot, bool force = false)
        {
            if(!force && datamodel.state.activeWeaponSlot == slot) return;
            datamodel.state.actions.Add(new DrawWeaponAction(character, slot));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void HolsterWeapon()
        {
            if(datamodel.state.animLock) return;
            datamodel.state.actions.Add(new HolsterWeaponAction(character));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void PrimaryFire(bool held)
        {
            if(datamodel.state.animLock) return;
            if(datamodel.state.isPrimaryFiring != held)
            {
                Abort();
                datamodel.state.isPrimaryFiring.Set(held);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Reload()
        {
            if(datamodel.state.animLock) return;
            Abort();
            datamodel.state.actions.Add(new ReloadWeaponAction(character));
        }

        //
        // --------------------------------------------------------------------
        //

        public void PackMagazine(MagazineItem mag, InventoryItem ammo)
        {
            if(datamodel.state.animLock) return;
            Abort();
            datamodel.state.actions.Add(new PackMagazineAction(character, mag, ammo));
        }

        //
        // ====================================================================
        //
        
        public void Despawn(float delaySec)
        {
            StartCoroutine(DespawnCoroutine(delaySec));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Abort()
        {
            if(ArmoryPanel.activePanel != null)
            {
                ArmoryPanel.activePanel.Hide();
            }

            datamodel.state.actions.CancelAll();
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        [ContextMenu("Collect Damage Areas")]
        protected void CollectDamageAreas()
        {
            damageAreas = gameObject.GetComponentsInChildren<DamageArea>();
            #if UNITY_EDITOR
                gameObject.UpdatePrefab();
            #endif
        }
        
        //
        // --------------------------------------------------------------------
        //

        protected void UpdateState()
        {
            CharacterState state = datamodel.state;
            if(state.health <= 0)
            {
                state.isAlive = false;
                Despawn(5f);
            }
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator DespawnCoroutine(float delaySec)
        {
            yield return new WaitForSeconds(delaySec);
            Destroy(character.gameObject);
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        private void OnTakeDamage(IDamageSource source, int amount)
        {
            CharacterState state = datamodel.state;
            state.health -= amount;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Update()
        {
            if(!initialized) return;
            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            if(!state.isAlive) return;

            state.actions.Update();

            //
            // basic movement
            //

            Vector3 delta = state.movedir.smoothed * state.moveSpeed;

            //
            // handle jumping
            //

            if(state.isJumping)
            {
                double elapsed = state.isJumping.elapsed;
                float jump = (float)stats.jumpCurve.Evaluate((float)elapsed) * stats.jumpPower;

                if(elapsed >= stats.jumpCurve.GetDuration())
                {
                    state.isJumping.Set(false);
                }
                delta += Vector3.up * jump;
            }
            //else
            {
                delta -= Vector3.up * stats.fallSpeed;
            }
            

            //
            // handle look yaw
            //
            character.transform.localRotation = Quaternion.Euler(0f, state.lookdir.x, 0f);

            //
            // apply movement
            //

            unityController.Move(delta*Time.deltaTime);

            bool wasGrounded = state.isGrounded;
            state.isGrounded.Set(unityController.isGrounded);

            if(state.isGrounded != wasGrounded)
            {
                character.audio.Play(AudioTrigger.Land);
            }

            if(state.isGrounded)
            {
                if(state.isJumping && state.isJumping.elapsed > 0.2f)
                {
                    state.isJumping.Set(false);
                }
            }
            
            UpdateState();
        }
    }
}
