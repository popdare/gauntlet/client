//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public interface IDamageSource
    {
    }

    //
    // ########################################################################
    //
    
    public interface IDamageReceiver
    {
        DamageSurfaceType surfaceType                           { get; }
        GameObject splatterFx                                   { get; }

        void TakeDamage(IDamageSource source, int amount);
    }
}
