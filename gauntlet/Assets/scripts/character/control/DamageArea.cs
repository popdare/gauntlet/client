//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class DamageArea
        : MonoBehaviour
        , IDamageReceiver
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void TakeDamageCallback(IDamageSource source, int amount);
        public event TakeDamageCallback OnTakeDamage;
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [SerializeField] private DamageSurfaceType _surfaceType = DamageSurfaceType.Default;
        public DamageSurfaceType surfaceType                    { get { return _surfaceType; } }

        [SerializeField] private GameObject _splatterFx         = null;
        public GameObject splatterFx                            { get { return _splatterFx; } }

        public float damageMultiplier                           = 1f;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void TakeDamage(IDamageSource source, int amount)
        {
            amount = (int)Mathf.Floor(amount * damageMultiplier);
            NotifyTakeDamage(source, amount);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void NotifyTakeDamage(IDamageSource source, int amount)
        {
            TakeDamageCallback cb = OnTakeDamage;
            if(cb != null)
            {
                cb(source, amount);
            }
        }
    }
}
