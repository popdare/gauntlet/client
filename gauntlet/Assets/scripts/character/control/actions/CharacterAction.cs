//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public enum CharacterActionType
    {
        HolsterWeapon,
        DrawWeapon,
        ReloadWeapon,
        PackMagazine
    }

    public abstract class CharacterAction
        : Coroutine
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Character character                              { get; private set; }
        public CharacterDatamodel datamodel                     { get { return character.datamodel; } }
        public abstract CharacterActionType type                { get; }

        public virtual bool isInterruptable                     { get { return true; } }

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public CharacterAction(Character character)
        {
            this.character = character;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual bool Cancel()
        {
            HandleComplete();
            return true;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        public virtual void OnPushed()
        {
        }
    }
}
