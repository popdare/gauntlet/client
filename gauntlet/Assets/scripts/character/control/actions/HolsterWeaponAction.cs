//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class HolsterWeaponAction
        : CharacterAction
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override CharacterActionType type                { get { return CharacterActionType.HolsterWeapon; } }
        public override bool isInterruptable                    { get { return false; } }

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public HolsterWeaponAction(Character character)
            : base(character)
        {
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override IEnumerator OnUpdate()
        {
            CharacterState state = datamodel.state;
            WeaponController weapon = datamodel.state.activeWeapon;
            if(weapon != null)
            {
                yield return weapon.OnHolster();
                GameObject.Destroy(weapon.gameObject);
                datamodel.state.activeWeapon = null;
                datamodel.events.NotifyWeaponChanged(character, weapon);
            }
        }
        
    }
}
