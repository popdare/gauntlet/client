//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class ReloadWeaponAction
        : CharacterAction
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override CharacterActionType type                { get { return CharacterActionType.HolsterWeapon; } }

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public ReloadWeaponAction(Character character)
            : base(character)
        {
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override IEnumerator OnUpdate()
        {
            if(datamodel.state.activeWeapon != null)
            {
                yield return datamodel.state.activeWeapon.Reload();
            }
        }
        
    }
}
