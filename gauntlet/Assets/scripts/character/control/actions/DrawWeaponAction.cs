//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;

namespace blacktriangles
{
    public class DrawWeaponAction
        : CharacterAction
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override CharacterActionType type                { get { return CharacterActionType.DrawWeapon; } }
        public override bool isInterruptable                    { get { return false; } }
        
        public WeaponSlot slot                                  { get; private set; }
        
        //
        // constructor ////////////////////////////////////////////////////////
        //

        public DrawWeaponAction(Character character, WeaponSlot slot)
            : base(character)
        {
            this.slot = slot;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnPushed()
        {
            base.OnPushed();
            character.datamodel.state.actions.Add(new HolsterWeaponAction(character));
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        

        protected override IEnumerator OnUpdate()
        {
            CharacterState state = datamodel.state;
            WeaponItem weaponItem = datamodel.armory.weapons[slot];
            if(weaponItem != null)
            {
                WeaponController weapon = weaponItem.CreateController(character);
                datamodel.events.NotifyWeaponChanged(character, weapon);
               
                yield return weapon.OnDraw();
                datamodel.state.activeWeapon = weapon;
                datamodel.state.activeWeaponSlot = slot;
            }
        }
    }
}
