//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;

namespace blacktriangles
{
    public class CharacterActionQueue
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public CharacterAction active                           { get; private set; }
        private Queue<CharacterAction> actions                  = new Queue<CharacterAction>();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool IsActive(CharacterActionType type)
        {
            return active != null && active.type == type;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(active == null || active.completed)
            {
                if(actions.Count > 0)
                {
                    active = actions.Dequeue();
                }
                else
                {
                    active = null;
                    return;
                }
            }

            if(active != null)
            {
                active.Update();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Add(CharacterAction action)
        {
            action.OnPushed();

            if(active == null && actions.Count == 0)
            {
                active = action;
            }
            else
            {
                actions.Enqueue(action);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public bool CancelCurrent()
        {
            if(Cancel(active))
            {
                active = null;
            }

            return active == null;
        }

        //
        // --------------------------------------------------------------------
        //

        public void CancelAll()
        {
            CancelCurrent();
            foreach(CharacterAction action in actions)
            {
                Cancel(action);
            }
            actions.Clear();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private bool Cancel(CharacterAction action)
        {
            bool result = false;
            if(action != null && action.isInterruptable)
            {
                result = action.Cancel();
            }

            return result;
        }
        
        
    }
}
