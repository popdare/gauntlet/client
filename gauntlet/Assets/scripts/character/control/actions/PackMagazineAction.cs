//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class PackMagazineAction
        : CharacterAction
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override CharacterActionType type                { get { return CharacterActionType.PackMagazine; } }
        public MagazineItem magazine                            { get; private set; }
        public InventoryItem ammo                               { get; private set; }

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public PackMagazineAction(Character character, MagazineItem mag, InventoryItem ammo)
            : base(character)
        {
            this.magazine = mag;
            this.ammo = ammo;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override IEnumerator OnUpdate()
        {
            datamodel.state.animLock.Lock();

            character.audio.Play(AudioTrigger.PackMagazineStart);
            CharacterStats stats = datamodel.stats;
            double wait = magazine.data.ammoPackingSpeed * stats.ammoPackSpeedMod;
            int count = System.Math.Min(ammo.count, magazine.inventory.remaining);

            float overallProg = 0.0f;
            float innerProg = 0.0f;
            for(int i = 0; i < count; ++i)
            {
                overallProg = i / (float)count;
                if(character.hasCamera)
                {
                    yield return WaitForSeconds(wait, (double elapsed)=>{
                        innerProg = (float)elapsed / (float)wait;
                        ProgressPanel.activePanel.SetProgress(innerProg, overallProg);
                    });
                }
                else
                {
                    yield return WaitForSeconds(wait);
                }

                character.audio.Play(AudioTrigger.PackMagazine);
                magazine.inventory.Add(InventoryItem.Create(character, ammo.data, 1));
                --ammo.count;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void HandleComplete()
        {
            ProgressPanel.activePanel.SetProgress(0f);
            datamodel.state.animLock.Unlock();
        }
    }
}
