//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class Character
        : MonoBehaviour
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum Type
        {
            Local,
            Remote,
            Doll
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int pid                                          { get; private set; }
        public Type type                                        = Type.Doll;

        public bool initializeOnStart                           = false;
    
        //
        // components ---------------------------------------------------------
        //
    
        public CharacterDatamodel datamodel                     { get; private set; }
        public CharacterControl controller                      { get; private set; }
        public CharacterView view                               { get; private set; }
        public CharacterDriver driver                           { get; private set; }
        #if UNITY_EDITOR
        public new CharacterAudio audio                         { get; private set; }
        #else
        public CharacterAudio audio                             { get; private set; }
        #endif
    
        public IEnumerable<CharacterComponent> components
        {
            get
            {
                yield return datamodel;
                yield return controller;
                yield return driver;
                yield return view;
                yield return audio;
            }
        }
        
        //
        // ICameraController --------------------------------------------------
        //
    
        public btCamera cam                                         { get; private set; }
        public bool hasCamera                                       { get { return cam != null; } }
    
        //
        // constructor / initializer //////////////////////////////////////////
        //

        public static Character Spawn(Character prefab)
        {
            return Spawn(prefab, Vector3.zero, Quaternion.identity);
        }

        //
        // --------------------------------------------------------------------
        //


        public static Character Spawn(Character prefab, Transform spawn)
        {
            return Spawn(prefab, spawn.position, spawn.rotation);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Character Spawn(
                Character prefab, 
                Vector3 position, 
                Quaternion rotation,
                Type type = Type.Local,
                int pid = -1
            )
        {
            Character character = Instantiate(prefab, position, rotation) as Character;
            character.pid = pid;
            character.type = type;
            return character;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Initialize()
        {
            foreach(CharacterComponent component in components)
            {
                if(component != null)
                {
                    component.Initialize(this);
                }
            }
        }
    
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void InstallDriver<DriverType>()
            where DriverType: CharacterDriver
        {
            driver = gameObject.AddComponent<DriverType>();
            driver.Initialize(this);
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Awake()
        {
            datamodel = gameObject.GetComponent<CharacterDatamodel>();
            controller = gameObject.GetComponent<CharacterControl>();
            driver = gameObject.GetComponent<CharacterDriver>();
            view = gameObject.GetComponent<CharacterView>();
            audio = gameObject.GetComponent<CharacterAudio>();
        }
    
        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            if(initializeOnStart)
            {
                Initialize();
            }
        }
        
        //
        // ICameraController //////////////////////////////////////////////////
        //
    
        public void OnTakeCamera(btCamera _cam)
        {
            cam = _cam;
            foreach(CharacterComponent comp in components)
            {
                if(comp != null)
                {
                    ICameraController cont = comp as ICameraController;
                    if(cont != null)
                    {
                        cont.OnTakeCamera(_cam);
                    }
                }
            }
        }
    
        //
        // --------------------------------------------------------------------
        //
        
        public void OnReleaseCamera()
        {
            cam = null;
            foreach(CharacterComponent comp in components)
            {
                if(comp != null)
                {
                    ICameraController cont = comp as ICameraController;
                    if(cont != null)
                    {
                        cont.OnReleaseCamera();
                    }
                }
            }
        }
    }
}
