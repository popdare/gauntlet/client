//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{

    public class CharacterComponent
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public bool initialized                                 { get; private set; }
        
        public Character character                              { get; private set; }
        public CharacterDatamodel datamodel                     { get; private set; }
        public CharacterEvents events                           { get { return datamodel.events; } }
    
        //
        // constructor / initializer //////////////////////////////////////////
        //
        
        public virtual void Initialize(Character _character)
        {
            character = _character;
            datamodel = character.datamodel;
            initialized = true;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Awake()
        {
            initialized = false;
        }
    }

}
