//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public enum AudioTrigger
    {
        OpenInventory,
        CloseInventory,
        PackMagazineStart,
        PackMagazine,

        Footstep,
        Jump,
        Land
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public class CharacterAudio
        : CharacterComponent
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public class AudioTriggerMap
            : EnumList<AudioTrigger, AudioVariation>
        {
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Header("Audio Mapping")]
        [SerializeField] AudioTriggerMap audioMapPrefabs        = null;
        [SerializeField] Transform audioRoot                    = null;

        [Header("Footsteps")]
        public double footstepTime                              = 1;

        private AudioTriggerMap audiomap                        = null;

        private double lastFootstep                             = 0;

        //
        // initialize /////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);

            if(audioRoot == null)
                audioRoot = transform;

            audiomap = new AudioTriggerMap();
            audioMapPrefabs.ForEach((AudioTrigger trig, AudioVariation prefab)=>{
                audiomap[trig] = Instantiate(prefab, audioRoot.position, audioRoot.rotation, audioRoot);
            });
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Play(AudioTrigger trigger)
        {
            AudioVariation vary = audiomap[trigger];
            if(vary != null)
            {
                vary.Play();
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            if(state.isMoving && state.isGrounded)
            {
                if(Epoch.now - lastFootstep > footstepTime * (1f/state.moveSpeed))
                {
                    lastFootstep = Epoch.now;
                    Play(AudioTrigger.Footstep);
                }
            }
        }
        
    }
}

