//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{

    public class CharacterDriver
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public CharacterControl controller                          { get; private set; }
    
        //
        // ////////////////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            controller = character.controller;
        }
    }

}
