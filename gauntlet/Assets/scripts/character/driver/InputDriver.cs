//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class InputDriver
        : CharacterDriver
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        private enum PlayerAction
        {
            MoveX,
            CounterStrafe,
            MoveY,
            LookX,
            LookY,
            Jump,
            Walk,
            DrawPrimary,
            DrawSecondary,
            DrawSidarm,
            PrimaryFire,
            Crouch,
            ADS,
            Reload,
        };

        //
        // members ////////////////////////////////////////////////////////////
        //

        private InputRouter<PlayerAction> input;

        //
        // initializer ////////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);
            input = new InputRouter<PlayerAction>();
            input.Bind(PlayerAction.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(PlayerAction.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(PlayerAction.CounterStrafe, InputAction.FromCombo(KeyCode.A, KeyCode.D));
            input.Bind(PlayerAction.CounterStrafe, InputAction.FromCombo(KeyCode.W, KeyCode.S));
            input.Bind(PlayerAction.LookX, InputAction.FromAxis(InputAction.Axis.MouseHorizontal, false, 1f));
            input.Bind(PlayerAction.LookY, InputAction.FromAxis(InputAction.Axis.MouseVertical, true, 1f));
            input.Bind(PlayerAction.Jump, InputAction.FromKey(KeyCode.Space));
            input.Bind(PlayerAction.Walk, InputAction.FromKey(KeyCode.LeftShift));
            input.Bind(PlayerAction.DrawPrimary, InputAction.FromKey(KeyCode.Alpha1));
            input.Bind(PlayerAction.DrawSecondary, InputAction.FromKey(KeyCode.Alpha2));
            input.Bind(PlayerAction.DrawSidarm, InputAction.FromKey(KeyCode.Alpha3));
            input.Bind(PlayerAction.PrimaryFire, InputAction.FromKey(KeyCode.Mouse0));
            input.Bind(PlayerAction.Crouch, InputAction.FromKey(KeyCode.LeftControl));
            input.Bind(PlayerAction.ADS, InputAction.FromKey(KeyCode.Mouse1));
            input.Bind(PlayerAction.Reload, InputAction.FromKey(KeyCode.R));
        }

        //
        // unity callbacks //////////////////////////////////////////////////// 
        //
        
        protected virtual void Update()
        {
            if(!initialized || !datamodel.state.isAlive) return;
            if(SceneManager.instance.uiActive) 
            {
                controller.HardStop();
                return;
            }

            //
            // MOVMEMENT
            //

            if(input.GetKeyPressed(PlayerAction.CounterStrafe))
            {
                controller.HardStop();
            }

            Vector2 move = new Vector2(
                input.GetAxis(PlayerAction.MoveX),
                input.GetAxis(PlayerAction.MoveY));

            controller.Move(move);
            controller.Walk(input.GetKey(PlayerAction.Walk));
            if(input.GetKeyPressed(PlayerAction.Crouch))
            {
                controller.ToggleCrouch();
            }

            Vector2 look = new Vector2(
                input.GetAxis(PlayerAction.LookX),
                input.GetAxis(PlayerAction.LookY)) * Time.deltaTime;

            controller.Look(look);

            if(input.GetKeyPressed(PlayerAction.Jump))
            {
                controller.Jump();
            }

            //
            // WEAPON
            //

            controller.SetAimDownSights(input.GetKey(PlayerAction.ADS));

            if(input.GetKeyPressed(PlayerAction.DrawPrimary))
            {
                controller.DrawWeapon(WeaponSlot.Primary);
            }
            else if(input.GetKeyPressed(PlayerAction.DrawSecondary))
            {
                controller.DrawWeapon(WeaponSlot.Secondary);
            }
            else if(input.GetKeyPressed(PlayerAction.DrawSidarm))
            {
                controller.DrawWeapon(WeaponSlot.Sidearm);
            }

            if(input.GetKeyPressed(PlayerAction.Reload))
            {
                controller.Reload();
            }

            controller.PrimaryFire(input.GetKey(PlayerAction.PrimaryFire));
        }
    }
}
