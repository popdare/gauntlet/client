//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using SubjectNerd.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class PathDriver
        : CharacterDriver
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct PathNode
        {
            public enum Facing
            {
                Movement,
                Player,
                Arbitrary
            };

            public Transform transform;
            public bool jump;
            public bool walk;
            public Facing facing;
            public bool isArbitraryFacing()                     { return facing == Facing.Arbitrary; }
            [ShowIf("isArbitraryFacing")] public Vector3 faceDir;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Reorderable("Item")] public List<PathNode> path;
        public WrapMode wrapMode                                = WrapMode.PingPong;

        private bool finished                                   = true;
        private int idx                                         = 0;
        private bool reverse                                    = false;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            controller.Teleport(path[0].transform.position);
            controller.DrawAnyWeapon(true);
            finished = false;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(finished) return;
            if(path.Count <= 0) return;
            if(path.IsValidIndex(idx) == false)
            {
                switch(wrapMode)
                {
                    case WrapMode.Loop: idx = 0; break;
                    case WrapMode.PingPong: reverse = !reverse; break;

                    default:
                        finished = true;
                        return;

                }
            }

            PathNode currentNode = path[idx];
            Vector3 diff = currentNode.transform.position - character.transform.position;
            float dist2 = diff.sqrMagnitude;

            controller.Walk(currentNode.walk);

            if(dist2 <= 1f)
            {
                if(currentNode.jump)
                    controller.Jump();
                if(reverse)
                    --idx;
                else
                    ++idx;
            }
            else
            {
                controller.MoveAbs(diff.normalized);
            }

            switch(currentNode.facing)
            {
                case PathNode.Facing.Player:
                    Vector3 playerDiff = PlaygroundSceneManager.instance.localCharacter.transform.position - character.transform.position;
                    controller.LookTowards(playerDiff.normalized);
                break;

                case PathNode.Facing.Movement:
                    controller.LookTowards(diff.normalized);
                break;

                default:
                    controller.LookTowards(currentNode.faceDir);
                break;
            }
        }
    }
}
