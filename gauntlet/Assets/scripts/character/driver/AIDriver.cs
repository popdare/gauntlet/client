//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.AI;

namespace blacktriangles
{
    public class AIDriver
        : CharacterDriver
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public new CharacterNavMeshControl controller           { get { return base.controller as CharacterNavMeshControl; } }

        public float wanderRange                                = 10f;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(!initialized || !datamodel.state.isAlive) return;

            if(!controller.agent.hasPath || controller.agent.remainingDistance <= 1f)
            {
                Vector3 randomPoint = character.transform.position + Random.insideUnitSphere * wanderRange;
                NavMeshHit hit;
                if(NavMesh.SamplePosition(randomPoint, out hit, 1f, NavMesh.AllAreas))
                {
                    controller.SetDestination(hit.position);
                }
            }
        }
        
    }
}
