//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class CharacterState
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public class TimestampedBool
            : TimestampedValue<bool>
        {
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public class SmoothVector3
            : SmoothValue<Vector3>
        {
            public SmoothVector3(int history)
                : base(history)
            {
            }
        }
        
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Character character                              { get; private set; }
        
        [Header("Locomotion")]
        public SmoothVector3 movedir;
        public Vector2 lookdir                                  = Vector2.zero;
        public TimestampedBool isMoving                         = new TimestampedBool();
        public TimestampedBool isJumping                        = new TimestampedBool();
        public TimestampedBool isGrounded                       = new TimestampedBool();
        public TimestampedBool isWalking                        = new TimestampedBool();
        public TimestampedBool isCrouching                      = new TimestampedBool();

        [Header("Health")]
        public int health                                       = 0;
        public bool isAlive                                     = true;

        [Header("Weapons")]
        public WeaponSlot activeWeaponSlot                      = WeaponSlot.Primary;
        public WeaponController activeWeapon                    = null;
        public TimestampedBool isAiming                         = new TimestampedBool();
        public TimestampedBool isPrimaryFiring                  = new TimestampedBool();

        [Header("Visualization")]
        public CharacterActionQueue actions                     = new CharacterActionQueue();
        public LockStack animLock                               = new LockStack();

        //
        // derived
        //

        public float moveSpeed                                  { get { return GetMoveSpeed(); } }

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public CharacterState(Character character, CharacterStats stats)
        {
            this.character = character;
            health = stats.maxHealth;
            movedir = new SmoothVector3(stats.moveSmoothing);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private float GetMoveSpeed()
        {
            CharacterStats stats = character.datamodel.stats;

            if(!isMoving) return 0f;

            float move = stats.moveSpeed;

            if(isAiming)
            {
                move *= stats.aimMoveModifier;
            }

            //
            // walking
            //

            if(isWalking)
            {
                move *= stats.walkSpeedModifier;
            }

            //
            // crouching
            //

            if(isCrouching)
            {
                move *= stats.crouchSpeedModifier;

            }

            //
            // grounded / airspeed
            //

            if(isGrounded == false)
            {
                move *= stats.airSpeedModifier;
            }

            return move;
        }
    }
}
