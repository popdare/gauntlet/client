//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class CharacterEvents
    {
        //
        // events /////////////////////////////////////////////////////////////
        //
        
        public delegate void EquipmentChangedCallback(Character character, InventoryItem prev, InventoryItem curr);
        public event EquipmentChangedCallback OnEquipmentChanged;

        public delegate void WeaponChangedCallback(Character character, WeaponController controller);
        public event WeaponChangedCallback OnWeaponChanged;

        //
        // notify methods /////////////////////////////////////////////////////
        //
        
        public void NotifyEquipmentChanged(Character character, InventoryItem prev, InventoryItem curr)
        {
            EquipmentChangedCallback cb = OnEquipmentChanged;
            if(cb != null)
            {
                cb(character, prev, curr);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void NotifyWeaponChanged(Character character, WeaponController controller)
        {
            WeaponChangedCallback cb = OnWeaponChanged;
            if(cb != null)
            {
                cb(character, controller);
            }
        }
        
    }
}
