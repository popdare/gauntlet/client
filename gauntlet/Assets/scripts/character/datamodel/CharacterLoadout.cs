//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;

namespace blacktriangles
{
    [System.Serializable]
    public class CharacterLoadout
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct HeldItem
        {
            public EquipSlot slot;
            public InventoryItemData data;
            public int inventoryIdx;
            public int count;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct WeaponLoad
        {
            public WeaponSlot slot;
            public MagazineItemData magazine;
            public AmmoItemData ammo;
            public int ammoCount;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public WeaponDataList weapons;
        public EquipmentDataList equipment;
        public List<HeldItem> heldItems;
        public List<WeaponLoad> loads;
    }
}
