//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct CharacterStats
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [Header("MOVEMENT")]
        [Min(0.1f)]
        public float moveSpeed;

        [Min(0f)]
        public float backSpeedModifier;

        [Min(0f)]
        public float crouchSpeedModifier;

        [Min(0f)]
        public float airSpeedModifier;

        [Min(0f)]
        public float walkSpeedModifier;

        [Min(1)]
        public int moveSmoothing;
        public AnimationCurve moveAcceleration;

        [Min(0f)]
        public float aimMoveModifier;

        //
        // --------------------------------------------------------------------
        //

        [Header("Look / Mouse")]
        public Vector2 lookSpeed;

        public bool enableMouseAcceleration;
        [ShowIf("enableMouseAcceleration")] public AnimationCurve mouseAcceleration;

        //
        // --------------------------------------------------------------------
        //
        
        [Header("Jumping")]
        public float jumpPower;
        public AnimationCurve jumpCurve;
        public float fallSpeed;

        //
        // --------------------------------------------------------------------
        //

        [Header("Size")]
        public float height;
        public float crouchHeight;
        public AnimationCurve crouchSpeed;

        //
        // --------------------------------------------------------------------
        //
        
        [Header("COMBAT")]
        public int maxHealth;

        //
        // --------------------------------------------------------------------
        //
        
        [Header("WEAPON")]
        public float aimSpeedMod;
        public float armSpeedMod;
        public float reloadSpeedMod;
        public float ammoPackSpeedMod;
        public float fireSpeedMod;
        public float aimRecoilModifier;
        public float crouchRecoilModifier;


        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public static CharacterStats Default { get {
            return new CharacterStats() {
                //
                // ------------------------------------------------------------
                //

                moveSpeed = 10f,
                backSpeedModifier = 0.85f,
                crouchSpeedModifier = 0.25f,
                airSpeedModifier = 0.95f,
                walkSpeedModifier = 0.6f,
                moveSmoothing = 5,
                moveAcceleration = new AnimationCurve(),
                aimMoveModifier = 0.6f,

                //
                // ------------------------------------------------------------
                //

                lookSpeed = new Vector2(50,50),
                enableMouseAcceleration = false,
                mouseAcceleration = new AnimationCurve(),

                //
                // ------------------------------------------------------------
                //
                
                jumpPower = 5f,
                jumpCurve = new AnimationCurve(),
                fallSpeed = 1f,

                //
                // ------------------------------------------------------------
                //
                
                height = 1.7f,
                crouchHeight = 1f,

                //
                // ------------------------------------------------------------
                //
                
                maxHealth = 100,

                //
                // ------------------------------------------------------------
                //

                aimSpeedMod = 1f,
                armSpeedMod = 1f,
                reloadSpeedMod = 1f,
                ammoPackSpeedMod = 1f,
                fireSpeedMod = 1f,
                aimRecoilModifier = 0.8f,
                crouchRecoilModifier = 0.85f,
            };
        }}

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override string ToString()
        {
            return 
                $@"[CharacterStats]
                    moveSpeed = {moveSpeed}
                    backSpeedModifier = {backSpeedModifier}
                    crouchSpeedModifier = {crouchSpeedModifier}
                    airSpeedModifier = {airSpeedModifier}
                    walkSpeedModifier = {walkSpeedModifier}
                    moveSmoothing = {moveSmoothing}
                    aimMoveModifier = {aimMoveModifier}
                                        
                    //
                    // --------------------------------------------------------
                    //
                    
                    lookSpeed = {lookSpeed}
                    enableMouseAcceleration = {enableMouseAcceleration}
                    
                    //
                    // --------------------------------------------------------
                    //
                    
                    jumpPower = {jumpPower}
                    fallSpeed = {fallSpeed}

                    //
                    // --------------------------------------------------------
                    //

                    height = {height}
                    crouchHeight = {crouchHeight}

                    //
                    // --------------------------------------------------------
                    //
                    
                    maxHealth = {maxHealth}

                    //
                    // --------------------------------------------------------
                    //
                    
                    aimSpeedMod = {aimSpeedMod},
                    armSpeedMod = {armSpeedMod},
                    reloadSpeedMod = {reloadSpeedMod},
                    ammoPackSpeedMod = {ammoPackSpeedMod},
                    fireSpeedMod = {fireSpeedMod}
                    aimRecoilModifier = {aimRecoilModifier}
                    crouchRecoilModifier = {crouchRecoilModifier}
                ";

        }

        //
        // --------------------------------------------------------------------
        //

        public object Clone()
        {
            return MemberwiseClone();
        }
        
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    [System.Serializable]
    public class StatsModifierList
        : ClassModifierList<CharacterStats>
    {
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
}
