//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public interface IStatsModifier
    {
        CharacterStats CalculateStats(CharacterStats orig, CharacterStats curr);
    }
}
