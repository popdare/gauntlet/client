//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class CharacterDatamodel
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        //
        // events
        //

        public new CharacterEvents events                       = new CharacterEvents();

        //
        // state
        //

        [ReadOnly] public CharacterState state;
        [ReadOnly] public Armory armory                         = null;

        //
        // stats
        //

        public CharacterStats stats                             { get { return CalculateStats(); } }
        [SerializeField] private CharacterStats baseStats       = CharacterStats.Default;
        private CharacterStats cacheStats;
        private bool statsDirty               					= true;

        //
        // initialize /////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            state = new CharacterState(_character, stats);
            armory = new Armory(_character);

            events.OnEquipmentChanged += OnEquipmentChanged;
            events.OnWeaponChanged += OnWeaponChanged;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private CharacterStats CalculateStats()
        {
            if(statsDirty)
            {
                cacheStats = baseStats;
                if(armory != null)
                {
                    cacheStats = armory.CalculateStats(baseStats, cacheStats);
                }
                statsDirty = false;
            }

            return cacheStats;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        private void OnEquipmentChanged(Character character, InventoryItem prev, InventoryItem curr)
        {
            statsDirty = true;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void OnWeaponChanged(Character character, WeaponController controller)
        {
            statsDirty = true;
        }

        //
        // unity //////////////////////////////////////////////////////////////
        //

        #if FALSE
        protected virtual void OnGUI()
        {
            if(!character.hasCamera) return;

            if(state.actions.active == null)
            {
                GUILayout.Label("[None]");
            }
            else
            {
                GUILayout.Label($"[{state.actions.active.type}]");
            }
        }
        #endif

        //
        // --------------------------------------------------------------------
        //
        
        #if UNITY_EDITOR
        [ContextMenu("Flag Stats Dirty")]
        private void FlagStatsDirty()
        {
            statsDirty = true;
			Dbg.Log($"STATS DIRTY {statsDirty}");
        }
        #endif
    }

}
