//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class CharacterView
        : CharacterComponent
        , ICameraController
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public btCamera cam                                     { get; private set; }
        public bool hasCamera                                   { get { return cam != null; } }
        
        public CharacterBody body;
        public Animator animator                                { get { return body == null ? null : body.animator; } }
        public Transform cameraBone;
        public Transform cameraAttach;

        private Vector3 height                                  = Vector3.zero;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            height.y = character.datamodel.stats.height;
            SetupCallbacks();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void SetupCallbacks()
        {
            datamodel.events.OnWeaponChanged += OnWeaponChanged;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void UpdateAnimator(Animator anim)
        {
            CharacterState state = datamodel.state;
            anim.SetFloat("movex", state.movedir.smoothed.x);
            anim.SetFloat("movey", state.movedir.smoothed.y);
            anim.SetBool("ismoving", state.isMoving);
            anim.SetBool("isjumping", state.isJumping);
            anim.SetBool("isgrounded", state.isGrounded);
            anim.SetBool("iswalking", state.isWalking);
            anim.SetBool("isalive", state.isAlive);
            anim.SetBool("ispackingmag", state.actions.IsActive(CharacterActionType.PackMagazine));
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        private void OnWeaponChanged(Character c, WeaponController cont)
        {
            if(cont != null)
            {
                cont.transform.AttachAtOrigin(cameraAttach);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OnTakeCamera(btCamera cam)
        {
            if(character.type != Character.Type.Local) return;
            this.cam = cam;
            this.cam.transform.AttachAtOrigin(cameraAttach);
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnReleaseCamera()
        {
            cam = null;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(!initialized) return;

            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            if(body != null && hasCamera)
            {
                float from = stats.crouchHeight;
                float to = stats.height;
                
                if(state.isCrouching)
                {
                    from = stats.height;
                    to = stats.crouchHeight;
                }

                float diff = to - from;
                height.y = from + diff * (float)stats.crouchSpeed.Evaluate((float)state.isCrouching.elapsed);

                body.transform.localPosition = height;
            }

            //
            // update body animations /////////////////////////////////////////
            //
            if(animator != null)
            {
                UpdateAnimator(animator);
            }

            if(cameraAttach != null)
            {
                cameraAttach.transform.localRotation = Quaternion.Euler(state.lookdir.y, 0f, 0f);
            }
        }
    }
}
