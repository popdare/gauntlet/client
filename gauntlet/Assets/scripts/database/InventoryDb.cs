//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEditor;

namespace blacktriangles
{
    public class InventoryDb
        : DatabaseTable
    {
        //
        // constants //////////////////////////////////////////////////////////
        //
        
        private const string kInventoryItemPath                 = "inventory";

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public override string displayName                      { get { return "Inventory Database"; } }

        public InventoryItemData[] all;
        public EnumListCollection<InventoryItemType, InventoryItemData> items;

        //
        // public methods /////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            all = LoadAll<InventoryItemData>(kInventoryItemPath);
            foreach(InventoryItemData item in all)
            {
                items[item.type].Add(item);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public InventoryItemData GetItem(string id)
        {
            foreach(InventoryItemData item in all)
            {
                if(item.id == id) return item;
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public InventoryItemData GetItem(string id, InventoryItemType type)
        {
            foreach(InventoryItemData item in items[type])
            {
                if(item.id == id) return item;
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public T GetItem<T>(string id)
            where T: InventoryItemData
        {
            return GetItem(id) as T;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public T GetItem<T>(string id, InventoryItemType type)
            where T: InventoryItemData
        {
            return GetItem(id, type) as T;
        }
    }
}
