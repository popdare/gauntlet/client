//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct GameSettings
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        [System.Serializable]
        public struct Mouse
        {
            public CursorLockMode cursorMode;
            public bool visible;
        };

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Mouse mouse;

        //
        // default ////////////////////////////////////////////////////////////
        //
        
        public static GameSettings Default
        {
            get
            {
                GameSettings result = new GameSettings();
                result.mouse.cursorMode = CursorLockMode.Locked;
                result.mouse.visible = true;

                return result;
            }
        }
    };
}
