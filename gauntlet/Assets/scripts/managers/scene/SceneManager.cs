//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class SceneManager
        : BaseSceneManager
    {
        // events //////////////////////////////////////////////////////////////
        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        // members /////////////////////////////////////////////////////////////
        public static new SceneManager instance                 { get; private set; }

        public bool uiActive                                    = false;

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();
        }

        // protected methods ///////////////////////////////////////////////////
        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}
