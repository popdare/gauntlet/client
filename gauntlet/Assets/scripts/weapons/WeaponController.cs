//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class WeaponController
        : MonoBehaviour
        , IDamageSource
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Character character                              { get; private set; }
        public WeaponItem weaponItem                            { get; private set; }

        [Header("View")]
        public Animator animator;
        public GameObject magazineView                          = null;
        public WeaponAnimEventReceiver receiver;

        private InventoryItem lastMag                           = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Initialize(Character _character, WeaponItem item)
        {
            character = _character;
            weaponItem = item;
            receiver.OnWeaponAnimEvent += OnAnimEvent;

            GlobalGameObjectPool.Create(weaponItem.data.primaryFireFx, GameManager.instance.transform);
            OnInitialize();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public IEnumerator Reload()
        {
            if(weaponItem.data.usesMagazine)
            {
                bool foundMag = false;
                Inventory firstInv = null;
                InventoryItem firstMag = null;
                foreach(EquipmentItem equip in character.datamodel.armory.equipment)
                {
                    if(equip != null && equip.inventory != null)
                    {
                        foreach(InventoryItem item in equip.inventory)
                        {
                            if(weaponItem.CanLoadMagazine(item))
                            {
                                if(firstInv == null) firstInv = equip.inventory;
                                if(firstMag == null) firstMag = item;

                                if(lastMag == null)
                                {
                                    yield return SwapMags(equip.inventory, item);
                                    yield break;
                                }

                                if(lastMag == item)
                                {
                                    foundMag = true;
                                }
                                else if(foundMag)
                                {
                                    yield return SwapMags(equip.inventory, item);
                                    yield break;
                                }
                            }
                        }
                    }
                }

                if(firstInv != null && firstMag != null)
                {
                    yield return SwapMags(firstInv, firstMag);
                    yield break;
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual IEnumerator OnDraw()
        {
            CharacterStats stats = character.datamodel.stats;
            CharacterState state = character.datamodel.state;

            state.animLock.Lock();

            animator.SetFloat("armspeed", stats.armSpeedMod);
            animator.SetTrigger("draw");

            yield return state.animLock;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual IEnumerator OnHolster()
        {
            CharacterStats stats = character.datamodel.stats;
            CharacterState state = character.datamodel.state;

            state.animLock.Lock();

            animator.SetFloat("armspeed", stats.armSpeedMod);
            animator.SetTrigger("holster");

            yield return state.animLock;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected virtual void OnInitialize()
        {
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void PrimaryFire()
        {
            GlobalGameObjectPool.Take(
                weaponItem.data.primaryFireFx, 
                character.transform.position, 
                character.transform.rotation, 
                null
            );
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual IEnumerator SwapMags(Inventory inventory, InventoryItem mag)
        {
            CharacterState state = character.datamodel.state;
            CharacterStats stats = character.datamodel.stats;
            state.animLock.Lock();

            animator.SetFloat("reloadspeed", stats.reloadSpeedMod);
            animator.SetTrigger("reload");

            yield return state.animLock;

            InventoryItem old = weaponItem.LoadMagazine(mag);
            inventory.Remove(mag);
            inventory.Add(old);
            lastMag = old;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        protected virtual void OnAnimEvent(Animator animator, WeaponAnimEventType type)
        {
            Dbg.Assert(animator == this.animator, "Got anim event for {animator.name} which does not match our animator!");
            switch(type)
            {
                case WeaponAnimEventType.DrawFinished: OnAnimationFinished(); break;
                case WeaponAnimEventType.HolsterFinished: OnAnimationFinished(); break;
                case WeaponAnimEventType.ReloadFinished: OnAnimationFinished(); break;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnAnimationFinished()
        {
            CharacterState state = character.datamodel.state;
            state.animLock.Unlock();
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            CharacterState state = character.datamodel.state;
            animator.SetBool("isaiming", state.isAiming);
            animator.SetBool("ispackingmag", state.actions.IsActive(CharacterActionType.PackMagazine));
            if(magazineView != null)
            {
                bool loaded = weaponItem.magazine != null;
                if(magazineView.activeSelf != loaded)
                {
                    magazineView.SetActive(loaded);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnDestroy()
        {
            receiver.OnWeaponAnimEvent -= OnAnimEvent;
        }
    }
}
