//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public enum SprayPatternType
    {
        Additive,
        Loop,
        Random,
    }

    [System.Serializable]
    [CreateAssetMenu(fileName="SprayPattern", menuName="blacktriangles/SprayPattern")]
    public class SprayPattern
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vector2[] points;
        public float scale                                      = 0.025f;
        public SprayPatternType type                            = SprayPatternType.Additive;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static Vector2 Next(SprayPattern data, ref int idx, ref Vector2 last)
        {
            Dbg.Assert(data.points.IsValidIndex(idx), $"Points index {idx} is invalid");
            Vector2 result = data.points[idx++] * data.scale;

            if(idx >= data.points.Length)
            {
                idx = 0;
            }

            switch(data.type)
            {
                case SprayPatternType.Additive:
                    result += last;
                    last = result;
                    break;

                case SprayPatternType.Loop:
                    break;

                case SprayPatternType.Random:
                    idx = data.points.RandomIndex();
                    break;
            }

            return result;
        }
    }
}
