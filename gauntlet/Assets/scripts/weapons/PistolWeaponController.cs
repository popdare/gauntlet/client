//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class PistolWeaponController
        : HitScanWeaponController
    {
        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override void PrimaryFire()
        {
            base.PrimaryFire();
            animator.SetTrigger("primaryfire");

            HitScanResult scan = HitScan();
            SpawnHitFx(scan);
            ApplyDamage(scan);
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void OnPrimaryFiringChanged(bool wasFiring, bool isFiring)
        {
            if(isFiring && CheckCanPrimaryFire())
            {
                PrimaryFire();
            }
        }
    }
}
