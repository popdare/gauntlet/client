//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class HitScanWeaponController
        : WeaponController
    {
        //
        // type ///////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Settings 
        {
            [Header("Firing Stats")]
            public float aimSpeed;
            public bool isFullyAuto;
            public float shotsPerSecond;
            public float shotDelay                              { get { return 1f / shotsPerSecond; } }

            [Header("Hit Scan")]
            public float penetration;
            public float penetrationCost;
            public float maxDistance;
            public LayerMask mask;

            [Header("Impact")]
            public int baseDamage;
            public DamageSurfaceFx hitfx;

            public static Settings Default                      { get {
                return new Settings() {
                    aimSpeed = 0.25f,
                    isFullyAuto = false,
                    shotsPerSecond = 1,

                    penetration = 0f,
                    penetrationCost = 10f,
                    maxDistance = 1000,
                    
                    baseDamage = 1,
                    hitfx = null,
                };
            }}
        }

        //
        // --------------------------------------------------------------------
        //

        public struct HitScanResult
        {
            public IDamageReceiver[] damageReceivers;
            public RaycastHit[] hits;
            public RaycastHit[] all;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public Transform scanSource;

        private Vector3 aimPoint                                = Vector3.zero;
        private double lastFired                                = 0f;
        private Settings settings                               = default(Settings);

        private int sprayIdx                                    = 0;
        private Vector2 sprayLast                               = Vector2.zero;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public HitScanResult HitScan()
        {
            //
            // cast ray
            //

            Vector3 aimDir = (aimPoint - scanSource.position).normalized;
            RaycastHit[] hits = Physics.RaycastAll(
                scanSource.position,
                aimDir,
                settings.maxDistance,
                settings.mask,
                QueryTriggerInteraction.UseGlobal
            );

            //
            // sort
            //

            System.Array.Sort(hits, (RaycastHit h1, RaycastHit h2)=>{
                float dist1 = (h1.point - scanSource.position).sqrMagnitude;
                float dist2 = (h2.point - scanSource.position).sqrMagnitude;

                return (int)(dist1-dist2)*100;
            });

            //
            // filter hits beyond our fist hit based on penetration
            // also see if any are damage targets.
            //

            List<RaycastHit> actualHits = new List<RaycastHit>();
            List<IDamageReceiver> targets = new List<IDamageReceiver>();
            if(hits.Length > 0)
            {
                RaycastHit prevHit = hits[0];
                actualHits.Add(prevHit);
                float pen = settings.penetration - settings.penetrationCost;
                for(int idx = 1; idx < hits.Length && pen > 0f; ++idx)
                {
                    RaycastHit nextHit = hits[idx];
                    float dist2 = (nextHit.point - prevHit.point).sqrMagnitude;

                    // if we wall banged this target...
                    if(dist2 < pen)
                    {
                        pen -= (settings.penetrationCost + dist2);
                        actualHits.Add(nextHit);
                        IDamageReceiver target = nextHit.collider.GetComponent<IDamageReceiver>();
                        if(target != null)
                        {
                            targets.Add(target);
                        }
                        prevHit = nextHit;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            //
            // return results
            //

                        foreach(RaycastHit hit in actualHits)
            {
                IDamageReceiver target = hit.collider.GetComponent<IDamageReceiver>();
                if(target != null)
                {
                    targets.Add(target);
                }
            }

            return new HitScanResult() {
                damageReceivers = targets.ToArray(),
                hits = actualHits.ToArray(),
                all = hits
            };
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override void OnInitialize()
        {
            character.datamodel.state.isPrimaryFiring.OnValueChanged += OnPrimaryFiringChanged;
            settings = weaponItem.data.hitScanSettings;
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void ApplyDamage(HitScanResult scan)
        {
            foreach(IDamageReceiver receiver in scan.damageReceivers)
            {
                receiver.TakeDamage(this, settings.baseDamage);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void SpawnHitFx(HitScanResult scan)
        {
            GameObject splatterFx = null;
            foreach(RaycastHit hit in scan.hits)
            {
                //
                // get the surface type
                //

                DamageSurfaceType surface = DamageSurfaceType.Default;
                IDamageReceiver target = hit.collider.GetComponent<IDamageReceiver>();
                if(target != null)
                {
                    surface = target.surfaceType;
                }

                //
                // spawn the effects (splatter comes from the previous hit)
                //

                Quaternion rot = Quaternion.LookRotation(hit.normal, Vector3.up);
                GlobalGameObjectPool.Take(settings.hitfx[surface], hit.point, rot, null);

                if(splatterFx != null)
                {
                    GameObject.Instantiate(splatterFx, hit.point, rot);
                }

                //
                // if we have a splatter effect use it on the next hit (if any)
                //

                if(target != null)
                {
                    splatterFx = target.splatterFx;
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual bool CheckCanPrimaryFire()
        {
            CharacterStats stats = character.datamodel.stats;

            if(weaponItem.data.usesMagazine)
            {
                if(weaponItem.magazine == null) 
                    return false;

                if(weaponItem.magazine.inventory.itemCount <= 0) return false;
            }

            return (Epoch.now - lastFired) >= settings.shotDelay * (1f/stats.fireSpeedMod);
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected override void PrimaryFire()
        {
            base.PrimaryFire();
            CharacterStats stats = character.datamodel.stats;
            CharacterState state = character.datamodel.state;

            if(weaponItem.data.usesMagazine)
            {
                weaponItem.magazine.inventory.Pop();
            }

            float recoilModifier = 1f;
            if(state.isCrouching)
                recoilModifier *= stats.crouchRecoilModifier;

            if(state.isAiming)
                recoilModifier *= stats.aimRecoilModifier;

            character.datamodel.state.lookdir -= SprayPattern.Next(
                weaponItem.data.sprayPattern,
                ref sprayIdx,
                ref sprayLast
            ) * recoilModifier;


            animator.SetFloat("firespeed", stats.fireSpeedMod * 2.0f);
            lastFired = Epoch.now;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        protected virtual void OnPrimaryFiringChanged(bool wasFiring, bool isFiring)
        {
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void OnDestroy()
        {
            base.OnDestroy();
            character.datamodel.state.isPrimaryFiring.OnValueChanged -= OnPrimaryFiringChanged;
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void Update()
        {
            CharacterStats stats = character.datamodel.stats;
            CharacterState state = character.datamodel.state;

            animator.SetFloat("aimspeed", stats.aimSpeedMod * settings.aimSpeed);
            base.Update();

            if(character.hasCamera)
            {
                RaycastHit hit;
                if(Physics.Raycast(character.cam.transform.position, 
                                   character.cam.transform.forward,
                                   out hit,
                                   settings.maxDistance,
                                   settings.mask,
                                   QueryTriggerInteraction.UseGlobal))
                {
                    aimPoint = hit.point;
                }
            }

            if(settings.isFullyAuto && character.datamodel.state.isPrimaryFiring && CheckCanPrimaryFire())
            {
                PrimaryFire();
            }
        }
    }
}
