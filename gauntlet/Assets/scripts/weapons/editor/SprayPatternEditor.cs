//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    [CustomEditor(typeof(SprayPattern))]
    public class SprayPatternEditor
        : UnityEditor.Editor
    {
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void OnEnable()
        {
            SceneView.duringSceneGui += OnSceneGUI;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }
        
        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected virtual void OnSceneGUI(SceneView view)
        {
            SprayPattern pattern = (SprayPattern)target;

            if(pattern == null) return;
            if(pattern.points == null) return;

            Vector3 prev = Vector3.zero;
            for(int idx = 0; idx < pattern.points.Length; ++idx)
            {
                Vector3 pos = pattern.points[idx].CloneWithZ(0f);
                pattern.points[idx] = Handles.PositionHandle(
                    pos,
                    Quaternion.identity
                ).ToVector2XY();

                if(pattern.type != SprayPatternType.Random)
                {
                    Handles.DrawLine(prev, pos);
                    Handles.Label(pos, $"{idx}");
                }
                prev = pos;
            }
        }
    }
}
